#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename "$0")" >>setup.log

function cloneOrUpdateLatest() {

  SEARCHSTR="${3:-*.*.*}"
  # LATESTVERSION=$(git -c 'versionsort.suffix=-' ls-remote --exit-code --refs --sort='version:refname' --tags $2 "${SEARCHSTR}" 2>/dev/null | tail --lines=1 | cut --delimiter='/' --fields=3)
  LATESTVERSION=$(git -c 'versionsort.suffix=-' ls-remote --exit-code --refs --sort='version:refname' $2 "${SEARCHSTR}" 2>/dev/null | tail --lines=1 | cut --delimiter='/' --fields=3)

  if [ -d "${1}" ]; then
    if [ -f ".${1}-install-info.sh" ]; then
      # shellcheck disable=SC1090
      source ".${1}-install-info.sh"
      # echo "$INDIINSTALLED : $INDILATEST"
      if [ "$INSTALLEDVERSION" == "$LATESTVERSION" ]; then
        # echo "same"
        install_action="noop"
      else
        # echo "different"
        install_action="update"
        rm -f ".${1}-install-info.sh"
        echo "export INSTALLEDVERSION=$LATESTVERSION" >>".${1}-install-info.sh"
      fi
    else
      # echo "no .install-info"
      echo "export INSTALLEDVERSION=$LATESTVERSION" >>".${1}-install-info.sh"

      install_action="clone"
    fi
  else
    echo "export INSTALLEDVERSION=$LATESTVERSION" >".${1}-install-info.sh"

    install_action="clone"
  fi

  cd ~/src || exit

  case $install_action in
  clone)
    git clone "$2"
    git switch "$LATESTVERSION"
    cd ~/src/"$1" || exit
    ;;
  update)
    cd ~/src/"$1" || exit
    git reset --hard
    git switch "$LATESTVERSION"
    git pull --ff-only
    ;;
  esac

  echo $install_action

}
