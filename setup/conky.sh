#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

#########################################################
#############  Configuration for System Monitoring

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# This will set you up with conky so that you can see how your system is doing at a moment's glance
# A big thank you to novaspirit who set up this theme https://github.com/novaspirit/rpi_conky
sudo apt -y install conky-all
cp "$DIR/conkyrc" ~/.conkyrc
sudo chown $SUDO_USER:$SUDO_USER ~/.conkyrc

#This should dynamically add lines to the conkyrc file based on the number of CPUs found
NUMEROFCPUS=$(grep -c ^processor /proc/cpuinfo)
for (( c=1; c<=$NUMEROFCPUS; c++ ))
do  
   CPULINE="CPU$c  \${cpu cpu$c}% \${cpubar cpu$c}"
   echo $CPULINE
   sed -i "/\${cpugraph DimGray DarkSlateGray} \$color/i $CPULINE" ~/.conkyrc
done

# This will put a link into the autostart folder so it starts at login
##################
mkdir -p ~/.config/autostart
sudo cat > ~/.config/autostart/startConky.desktop <<- EOF
[Desktop Entry]
Name=StartConky
Exec=conky -db -p 20
Terminal=false
Type=Application
EOF
##################
# Note that in order to work, this link needs to stay owned by root and not be executable