#!/usr/bin/env bash
# set -x

echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

rm -rf ~/Projects

# rm -rf ~/src/buildKstarsNightlyFromGit

mkdir -p src
cd ~/src
# This will clone or update the repo
if [ ! -d ~/src/buildKstarsNightlyFromGit ]
then
	git clone https://github.com/JerryBlack/buildKstarsNightlyFromGit.git
  cd buildKstarsNightlyFromGit
else
	cd ~/src/buildKstarsNightlyFromGit
  git reset --hard
	git pull --ff-only
fi


PROC=$(expr $(nproc) - 1)
INDILATEST=$(git -c 'versionsort.suffix=-' ls-remote --exit-code --refs --sort='version:refname' --tags http://github.com/indilib/indi.git '*.*.*' 2>/dev/null | tail --lines=1 | cut --delimiter='/' --fields=3)
INDI3PLATEST=$(git -c 'versionsort.suffix=-' ls-remote --exit-code --refs --sort='version:refname' --tags http://github.com/indilib/indi-3rdparty.git '*.*.*' 2>/dev/null | tail --lines=1 | cut --delimiter='/' --fields=3)
KSTARSLATEST=$(git ls-remote --exit-code --sort='version:refname' https://invent.kde.org/education/kstars.git/ 'stable-*' 2>/dev/null | tail --lines=1 | cut --delimiter='/' --fields=3)


if [ -f .install-info.sh ]; then
  source .install-info.sh
	# echo "$INDIINSTALLED : $INDILATEST"
  if [ $INDIINSTALLED == $INDILATEST -a $INDI3PINSTALLED == $INDI3PLATEST -a $KSTARSINSTALLED == $KSTARSLATEST ]; then
		# echo "same"
		install_action="noop"
	else
	  # echo "different"
		install_action="update"
		rm -f .install-info.sh
		echo "export INDIINSTALLED=$INDILATEST" >> .install-info.sh
	  echo "export INDI3PINSTALLED=$INDI3PLATEST" >> .install-info.sh
	  echo "export KSTARSINSTALLED=$KSTARSLATEST" >> .install-info.sh
	fi
else 
  # echo "no .install-info"
	echo "export INDIINSTALLED=$INDILATEST" >> .install-info.sh
	echo "export INDI3PINSTALLED=$INDI3PLATEST" >> .install-info.sh
	echo "export KSTARSINSTALLED=$KSTARSLATEST" >> .install-info.sh

	install_action="clone"
fi


# sed -i "s/jobs=-j2/jobs=-j${PROC}/" buildKstarsNightlyFromGit.txt
sed -i "s/#sudo make install/sudo make install/" buildKstarsNightlyFromGit.txt
sed -i "s/git clone http:\/\/github.com\/indilib\/indi.git/git clone --branch ${INDILATEST} --depth 1 http:\/\/github.com\/indilib\/indi.git/" buildKstarsNightlyFromGit.txt
sed -i "s/git clone http:\/\/github.com\/indilib\/indi-3rdparty.git/git clone --branch ${INDI3PLATEST} --depth 1 http:\/\/github.com\/indilib\/indi-3rdparty.git/" buildKstarsNightlyFromGit.txt
sed -i "s/git clone http:\/\/invent.kde.org\/education\/kstars.git/git clone --branch ${KSTARSLATEST} --depth 1 http:\/\/invent.kde.org\/education\/kstars.git/" buildKstarsNightlyFromGit.txt

# bash installgsc.sh

# bash buildKstarsNightlyFromGit.txt all
# bash buildKstarsNightlyFromGit.txt clone
# bash buildKstarsNightlyFromGit.txt update

case $install_action in
  clone)
		bash buildKstarsNightlyFromGit.txt all
		;;
  update)
		bash buildKstarsNightlyFromGit.txt all
		;;
	noop)
		echo "indi/kstars is up to date"
esac

# Copy missing indi-rpi xml file
# @see https://github.com/indilib/indi/issues/1658
sudo cp ~/Projects/indi-3rdparty/indi-rpicam/indi_rpicam.xml.cmake /usr/share/indi/indi_rpicam.xml