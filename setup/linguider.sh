#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

# http://sourceforge.net/projects/linguider/

sudo apt-get install libusb-1.0-0-dev
sudo apt-get install libqt4-dev
sudo apt-get install libftdi-dev
sudo apt-get install fxload

>>>> copy lin_guider-26.0_static.tar.bz2 to /home/pi
tar -xvf lin_guider-26.0_static.tar.bz2

cd lin_guider

>>>> copy lin_guider-27.0_sfx.tar.bz2 to /home/pi/lin_guider
tar -xvf lin_guider-27.0_sfx.tar.bz2

sudo ./lin_guider.bin

>>> To install QHY5 firmware
pi@raspberrypi ~/lin_guider/udev/qhy5 $ sudo sh qhy5_lg_install.sh

>>>> To run program
cd /home/pi/lin_guider/lin_guider_pack/lin_guider
./lin_guider