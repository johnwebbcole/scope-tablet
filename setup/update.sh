#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

sudo apt update
sudo apt -y upgrade

