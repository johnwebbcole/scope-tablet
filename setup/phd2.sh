#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

# Installs PHD2 if you want it.  If not, comment each line out with a #.
sudo apt -y install build-essential git cmake pkg-config libwxgtk3.0-gtk3-dev \
   wx-common wx3.0-i18n libindi-dev libnova-dev gettext zlib1g-dev libx11-dev \
   libcurl4-gnutls-dev
# display "Building and Installing PHD2"

if [ ! -d ~/src/phd2 ]
then
	cd ~/src/
	git clone --depth 1 https://github.com/OpenPHDGuiding/phd2.git
	mkdir -p ~/src/phd2-build
else
	cd ~/src/phd2
	git pull
fi

cd ~/src/phd2-build
cmake ~/src/phd2
make -j $(expr $(nproc) - 1)
sudo make install

# # display "Installing Dependencies for wxFormBuilder and PHD Log Viewer"
# sudo apt -y install libwxgtk3.0-gtk3-dev libwxgtk-media3.0-gtk3-dev meson

# # This code will build and install PHD Log Viewer.  If you don't want it, comment it out.
# # display "Building and Installing PHD Log Viewer"
# if [ ! -d ~/src/phdlogview ]
# then
# 	cd ~/src/
# 	git clone --depth 1 https://github.com/agalasso/phdlogview.git
# 	mkdir -p ~/src/phdlogview/tmp
# else
# 	cd ~/src/phdlogview
# 	git pull
# fi

# # Note that phdlogview.fbp needs to be updated to the latest version of wxFormBuilder.  This copy has been updated
# # so that it won't cause an error in the build.  As soon as the repo gets updated to be compatible, this next line can be removed.
# # cp -f $USERHOME/AstroPi3/phdlogview.fbp ~/src/phdlogview/phdlogview.fbp

# cd ~/src/phdlogview/tmp
# cmake -DHAVE_WXFB=0 ~/src/phdlogview
# make -j $(expr $(nproc) - 1)
# sudo cp ~/src/phdlogview/tmp/phdlogview /usr/bin/

# # This will make a shortcut to PHD Log Viewer.  If you aren't installing it, be sure to remove this too.
# ##################
# sudo cat > $USERHOME/Desktop/utilities/PHDLogViewer.desktop <<- EOF
# [Desktop Entry]
# Version=1.0
# Type=Application
# Terminal=false
# Icon[en_US]=phd2
# Name[en_US]=PHD Log Viewer
# Exec=/usr/bin/phdlogview
# Name=PHD Log Viewer
# Icon=phd2
# EOF
# ##################
# sudo chmod +x $USERHOME/Desktop/utilities/PHDLogViewer.desktop
# sudo chown $SUDO_USER:$SUDO_USER $USERHOME/Desktop/utilities/PHDLogViewer.desktop
