#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename "$0")" >>setup.log

# shellcheck source=../setup-utils.sh
source setup-utils.sh

cloneOrUpdateLatest stellarsolver https://github.com/rlancaste/stellarsolver.git "*.*"

mkdir -p ~/src/build
cd ~/src/build || exit

sudo apt -y install git cmake qtbase5-dev libcfitsio-dev libgsl-dev wcslib-dev
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=RelWithDebInfo -DBUILD_TESTER=ON ../stellarsolver/
make -j $(expr $(nproc) - 1)
sudo make install
