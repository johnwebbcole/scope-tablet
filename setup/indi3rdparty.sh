#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename "$0")" >>setup.log

# shellcheck source=../setup-utils.sh
source setup-utils.sh

sudo apt-get -y install libnova-dev libcfitsio-dev libusb-1.0-0-dev zlib1g-dev libgsl-dev build-essential cmake git libjpeg-dev libcurl4-gnutls-dev libtiff-dev libfftw3-dev libftdi-dev libgps-dev libraw-dev libdc1394-22-dev libgphoto2-dev libboost-dev libboost-regex-dev librtlsdr-dev liblimesuite-dev libftdi1-dev libavcodec-dev libavdevice-dev

mkdir -p ~/src
cd ~/src || exit

cloneOrUpdateLatest indi-3rdparty https://github.com/indilib/indi-3rdparty

## #Build all libs
mkdir -p ~/src/build/indi-3rdparty-libs
cd ~/src/build/indi-3rdparty-libs || exit
# cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug -DBUILD_LIBS=1 ~/src/indi-3rdparty
cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug -DBUILD_LIBS=1 ~/src/indi-3rdparty
make -j "$(("$(nproc)" - 1))"
sudo make install

### Build all drivers
mkdir -p ~/src/build/indi-3rdparty
cd ~/src/build/indi-3rdparty || exit
# cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug ~/Projects/indi-3rdparty
cmake -DCMAKE_INSTALL_PREFIX=/usr ~/src/indi-3rdparty
make -j "$(("$(nproc)" - 1))"
sudo make install

# Copy missing indi-rpi xml file
# @see https://github.com/indilib/indi/issues/1658
sudo cp $USERHOME/src/indi-3rdparty/indi-rpicam/indi_rpicam.xml.cmake /usr/share/indi/indi_rpicam.xml
