#!/usr/bin/env bash

# shellcheck source=../setup-utils.sh
source setup-utils.sh

sudo apt-get -y install build-essential cmake git libeigen3-dev libcfitsio-dev zlib1g-dev libindi-dev extra-cmake-modules libkf5plotting-dev libqt5svg5-dev libkf5xmlgui-dev libkf5kio-dev kinit-dev libkf5newstuff-dev libkf5doctools-dev libkf5notifications-dev qtdeclarative5-dev libkf5crash-dev gettext libnova-dev libgsl-dev libraw-dev libkf5notifyconfig-dev wcslib-dev libqt5websockets5-dev xplanet xplanet-images qt5keychain-dev libsecret-1-dev breeze-icon-theme

cd ~/src || exit

cloneOrUpdateLatest kstars https://invent.kde.org/education/kstars.git "stable-*.*.*"

rm -f ~/src/build/kstars
mkdir -p ~/src/build/kstars
cd ~/src/build/kstars || exit

pwd

cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=RelWithDebInfo ~/src/kstars
make -j "$(("$(nproc)" - 1))"
sudo make install
