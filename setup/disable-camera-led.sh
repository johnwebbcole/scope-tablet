#!/usr/bin/env bash
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

CONFIG=/boot/config.txt
sudo raspi-config nonint set_config_var disable_camera_led 1 $CONFIG