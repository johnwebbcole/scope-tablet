#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

mkdir -p src
cd ~/src
git clone https://github.com/johnwebbcole/AstroPi3.git