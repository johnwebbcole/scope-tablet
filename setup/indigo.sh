#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

  # sudo grep -s "consoleblank=0" /boot/cmdline.txt && echo "consoleblank is disabled" || {
  #   sudo sed -i '1 s/$/ consoleblank=0/' /boot/cmdline.txt
  # }

  if [ ! -f /etc/apt/sources.list.d/indigo.list ]; then
    echo "deb [trusted=yes] https://indigo-astronomy.github.io/indigo_ppa/ppa indigo main" | sudo tee -a /etc/apt/sources.list.d/indigo.list
    sudo apt-get update
  fi

  sudo apt -y install indigo
  sudo apt -y install ain-imager
  sudo apt -y install indigo-control-panel