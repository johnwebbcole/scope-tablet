
#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

mkdir -p src
cd ~/src
sudo apt install -y libtool intltool autoconf automake libcurl4-openssl-dev
sudo apt install -y pkg-config libglib2.0-dev libgtk-3-dev libgoocanvas-2.0-dev
git clone https://github.com/csete/gpredict.git
cd gpredict
./autogen.sh
make
sudo make install