#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log


if [ ! -d ~/src/virtualgps ]
then
	cd ~/src/
	git clone https://github.com/rkaczorek/virtualgps.git
  cd ~/src/virtualgps
else
	cd ~/src/virtualgps
	git pull
fi

sudo cp virtualgps.py /usr/bin/
sudo cp virtualgps.service /etc/systemd/system/

sudo systemctl daemon-reload
sudo systemctl enable virtualgps.service
sudo systemctl start virtualgps.service
sudo systemctl status virtualgps.service


