
#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

mkdir -p src
cd ~/src
git clone https://github.com/rkaczorek/gpspanel.git
cd gpspanel

$sudo pip3 install -r requirements.txt 

cat << EOF | sudo tee /etc/systemd/system/gpspanel.service
[Unit]
Description=GPS Panel
After=multi-user.target

[Service]
Type=idle
User=nobody
ExecStart=/usr/bin/python3 DIRECTORY/gpspanel/gpspanel.py
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF 

sudo cp gpspanel.service /etc/systemd/system/
sudo chmod 644 /etc/systemd/system/gpspanel.service

sudo systemctl daemon-reload
sudo systemctl enable gpspanel.service
sudo systenctl start gpspanel.service