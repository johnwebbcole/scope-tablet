#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >>setup.log

# This will clone or update the repo
if [ ! -d ~/src/indi-mqtt ]; then
	cd ~/src/
	git clone https://github.com/rkaczorek/indi-mqtt.git
else
	cd ~/src/indi-mqtt
	git pull
fi

# @see https://indilib.org/forum/astroberry/6120-mqtt-publisher-for-indi.html

sudo apt -y install python-setuptools python-dev libindi-dev swig libcfitsio-dev libnova-dev
sudo apt -y install python-setuptools python-dev libindi-dev swig libcfitsio-dev libnova-dev
sudo apt -y install swig libz3-dev libcfitsio-dev libnova-dev
sudo apt -y install python-setuptools python-dev libindi-dev swig libcfitsio-dev libnova-dev

sudo pip3 install pyindi-client paho-mqtt simplejson

sudo cp indi-mqtt.py /usr/bin/
sudo cp indi-mqtt.service /etc/systemd/system/
sudo cp indi-mqtt.conf /etc/

sudo systemctl daemon-reload
sudo systemctl enable indi-mqtt.service
sudo systemctl start indi-mqtt.service
sudo systemctl status indi-mqtt.service
