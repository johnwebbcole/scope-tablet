#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >>setup.log

# shellcheck source=../setup-utils.sh
source setup-utils.sh

sudo apt-get install -y \
  git \
  cdbs \
  dkms \
  cmake \
  fxload \
  libev-dev \
  libgps-dev \
  libgsl-dev \
  libraw-dev \
  libusb-dev \
  zlib1g-dev \
  libftdi-dev \
  libgsl0-dev \
  libjpeg-dev \
  libkrb5-dev \
  libnova-dev \
  libtiff-dev \
  libfftw3-dev \
  librtlsdr-dev \
  libcfitsio-dev \
  libgphoto2-dev \
  build-essential \
  libusb-1.0-0-dev \
  libdc1394-22-dev \
  libboost-regex-dev \
  libcurl4-gnutls-dev \
  libtheora-dev

mkdir -p ~/src
cd ~/src || exit

cloneOrUpdateLatest indi http://github.com/indilib/indi.git

mkdir -p ~/src/build/indi-core
cd ~/src/build/indi-core || exit
# cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug ~/src/indi
cmake -DCMAKE_INSTALL_PREFIX=/usr ~/src/indi
make -j "$(("$(nproc)" - 1))"
sudo make install
