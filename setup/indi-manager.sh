#!/usr/bin/env bash
set -x
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >>setup.log

# # install instrucitons from repo
# sudo apt-add-repository ppa:mutlaqja/ppa -y
# sudo apt update
# sudo apt -y install python3-pip
# pip3 install indiweb
# sudo apt -y install indiwebmanagerapp

# build from source instructions from repo
sudo apt -y install python3-pip
pip3 install indiweb
mkdir -p ~/AstroRoot/
cd ~/AstroRoot/ || exit
git clone https://github.com/rlancaste/INDIWebManagerApp.git
mkdir -p ~/AstroRoot/INDIWebManagerApp-build
cd ~/AstroRoot/INDIWebManagerApp-build || exit
cmake -DCMAKE_INSTALL_PREFIX=/usr ~/AstroRoot/INDIWebManagerApp/
make
sudo make install

# sudo pip install indiweb
cat <<EOF | sudo tee /etc/systemd/system/indiwebmanager.service
[Unit]
Description=INDI Web Manager
After=multi-user.target

[Service]
Type=idle
# MUST SET YOUR USERNAME HERE.
User=pi
ExecStart=/home/pi/.local/bin/indi-web -v
Restart=always
RestartSec=5

[Install]
WantedBy=multi-user.target
EOF

sudo chmod 644 /etc/systemd/system/indiwebmanager.service

sudo systemctl daemon-reload
sudo systemctl enable indiwebmanager.service
sudo systemctl start indiwebmanager.service
sudo systemctl status indiwebmanager.service

# #########################################################
# #############  INDI WEB MANAGER APP

# display "Building and Installing INDI Web Manager App, indiweb, and python3"

# # This will install pip3
# sudo apt -y install python3-pip

# # This will install indiweb as the user
# sudo -H -u $SUDO_USER pip3 install indiweb

# # This will clone or update the repo
# if [ ! -d $USERHOME/AstroRoot/INDIWebManagerApp ]
# then
# 	cd $USERHOME/AstroRoot/
# 	sudo -H -u $SUDO_USER git clone --depth 1 https://github.com/rlancaste/INDIWebManagerApp.git
# 	sudo -H -u $SUDO_USER mkdir -p $USERHOME/AstroRoot/INDIWebManagerApp-build
# else
# 	cd $USERHOME/AstroRoot/INDIWebManagerApp
# 	sudo -H -u $SUDO_USER git pull
# fi

# # This will make and install the program
# cd $USERHOME/AstroRoot/INDIWebManagerApp-build
# sudo -H -u $SUDO_USER cmake -DCMAKE_INSTALL_PREFIX=/usr $USERHOME/AstroRoot/INDIWebManagerApp/
# sudo -H -u $SUDO_USER make -j $(expr $(nproc) + 2)
# sudo make install

# # This will make a link to start INDIWebManagerApp on the desktop
# ##################
# sudo cat > $USERHOME/Desktop/INDIWebManagerApp.desktop <<- EOF
# [Desktop Entry]
# Encoding=UTF-8
# Name=INDI Web Manager App
# Type=Application
# Exec=INDIWebManagerApp %U
# Icon=$(sudo -H -u $SUDO_USER python3 -m site --user-site)/indiweb/views/img/indi_logo.png
# Comment=Program to start and configure INDI WebManager
# EOF
# ##################
# sudo chmod +x $USERHOME/Desktop/INDIWebManagerApp.desktop
# sudo chown $SUDO_USER:$SUDO_USER $USERHOME/Desktop/INDIWebManagerApp.desktop
# ##################
