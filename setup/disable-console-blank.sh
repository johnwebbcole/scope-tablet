#!/usr/bin/env bash
set -x

echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

# disable_consoleblank

sudo grep -s "consoleblank=0" /boot/cmdline.txt && echo "consoleblank is disabled" || {
  sudo sed -i '1 s/$/ consoleblank=0/' /boot/cmdline.txt
}