#!/usr/bin/env bash
set -x

mkdir -p ~/src
cd ~/src/ || exit 2

if [ ! -d picam3-minipitft ]; then 
  git clone https://gitlab.com/johnwebbcole/picam3-minipitft.git
fi

cd picam3-minipitft || exit 3

git checkout node

npm install
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

sudo npm i -g nodemon

sudo pip3 install adafruit-circuitpython-rgb-display
sudo pip3 install --upgrade --force-reinstall spidev
sudo apt-get -y install python3-pip 
sudo apt-get -y install python3-pil python3-numpy
npm install dejavu-fonts-ttf

# auto start picam3-minipitft
grep -s "picam3-minipitft" ~/.bashrc && echo "picam3-minipitft is configured in ~/.bashrc" || {
    cat << EOS >> ~/.bashrc

# start picam3-minipitft
if [ \$(tty) == /dev/tty1 ]; then
   pushd ~/src/picam3-minipitft
   sudo nodemon --exec "python3" screen.py >screen.log 2>&1 &
   sleep 1
   DEBUG=index,system,buttons nodemon index.js >minipitft.log 2>&1 &
   popd
fi
EOS
  }