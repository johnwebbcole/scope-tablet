#!/usr/bin/env bash
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

source raspi-config
declare -F
CONFIG=/boot/config.txt
sudo raspi-config nonint set_config_var start_x 1 $CONFIG
CUR_GPU_MEM=$(vcgencmd get_mem gpu | cut -d '=' -f 2 | cut -d 'M' -f 1)
if [ -z "$CUR_GPU_MEM" ] || [ "$CUR_GPU_MEM" -lt 256 ]; then
  sudo raspi-config set_config_var nonint gpu_mem 256 $CONFIG
fi