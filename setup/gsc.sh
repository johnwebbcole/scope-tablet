#!/usr/bin/env bash
echo "$(date +'%Y-%m-%dT%H:%M:%S.%3N%Z') $(basename $0)" >> setup.log

set -x
# Installs the General Star Catalog if you plan on using the simulators to test (If not, you can comment this line out with a #)
# display "Building and Installing GSC"
if [ ! -d /usr/share/GSC ]
then
	mkdir -p ~/src/gsc
	cd ~/src/gsc
	if [ ! -f ~/src/gsc/bincats_GSC_1.2.tar.gz ]
	then
		wget -O bincats_GSC_1.2.tar.gz http://cdsarc.u-strasbg.fr/viz-bin/nph-Cat/tar.gz?bincats/GSC_1.2
	fi
	tar -xvzf bincats_GSC_1.2.tar.gz
	cd ~/src/gsc/src
	make -j $(expr $(nproc) - 1)
	mv gsc.exe gsc
	sudo cp gsc /usr/bin/
	sudo cp -r ~/src/gsc /usr/share/
  sudo mkdir -p /usr/share/GSC/bin

	cd ~/src/gsc
  sudo mv ./N* /usr/share/GSC
  sudo mv ./S* /usr/share/GSC
	sudo mv ./bin/regions.* /usr/share/GSC/bin/
	# sudo mv /usr/share/gsc /usr/share/GSC
	# sudo rm -r /usr/share/GSC/bin-dos
	# sudo rm -r /usr/share/GSC/src
	# sudo rm /usr/share/GSC/bincats_GSC_1.2.tar.gz
	# sudo rm /usr/share/GSC/bin/gsc.exe
	# sudo rm /usr/share/GSC/bin/decode.exe
	
	if [ -z "$(grep 'export GSCDAT' ~/.profile)" ]
	then
		# cp /etc/profile /etc/profile.copy
		echo "export GSCDAT=/usr/share/GSC" >> ~/.profile
	fi
else
	echo "GSC is already installed"
fi