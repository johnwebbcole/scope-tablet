#!/bin/bash
set -e

# Run this command to setup WIFI on a newly imaged
# SD card.  It expects the card to be mounted in 
# `/Volumnes/boot`.  It adds a `wpa_supplicant.conf`
# file to connect to a wifi network on inital boot.
config_wifi() {
  read -p "wifi SSID:" WIFISSID
  read -p "$WIFISSID password:" WIFIPASSWORD
  
  touch /Volumes/boot/ssh
  cat << EOF > /Volumes/boot/wpa_supplicant.conf
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1
  country=US
  
  network={
      ssid="$WIFISSID"
      scan_ssid=1
      psk="$WIFIPASSWORD"
      key_mgmt=WPA-PSK
  }
EOF
  
  BOOTMOUNTDRIVE=$(diskutil list | grep "\bboot" | tr -s " " | cut -d " " -f7)
  diskutil unmount /dev/$BOOTMOUNTDRIVE
}

# Copies the `~/.ssh/id_rsa.pub` file to a new
# raspbery pi at `pi@raspberrypi.local` and configures
# ssh to use the public key.  It disables password
# authentication via SSH.  You can still log in locally
# with the default `pi` user password.
# Also prompts and sets the hostname of the pi.
config_ssh() {
  read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
  NEWHOST=${PROMTPHOST:-$DEFAULTHOST}
  scp ~/.ssh/id_rsa.pub pi@raspberrypi.local:~/

  ssh pi@raspberrypi.local <<EOF
mkdir -p .ssh
mv id_rsa.pub .ssh/authorized_keys
chmod 600 .ssh/authorized_keys
chmod 700 .ssh
EOF

  ssh -i ~/.ssh/id_rsa pi@raspberrypi.local <<EOF
echo "ok"
sudo sed -i "s/^#PasswordAuthentication yes$/PasswordAuthentication no/" /etc/ssh/sshd_config

sudo sed -i "s/^raspberrypi$/$NEWHOST/" /etc/hostname
sudo sed -i "s/raspberrypi$/$NEWHOST/" /etc/hosts
sudo reboot
EOF

until ping -c1 $NEWHOST.local &>/dev/null; do :; done

  ssh -i ~/.ssh/id_rsa pi@$NEWHOST.local <<EOF
hostname
sudo apt-get update
sudo apt-get upgrade -y
EOF
}

config_scopetablet() {
  read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
  NEWHOST=${PROMTPHOST:-$DEFAULTHOST}

  ssh -q -i ~/.ssh/id_rsa pi@$NEWHOST.local <<EOF
  set -x

  # install_node
  hash node 2> /dev/null || {
    curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
    sudo apt-get install -y nodejs build-essential
  }
  
  # install ser2net
  sudo apt-get install -y ser2net
  sudo grep -s "^4000:raw" /etc/ser2net.conf || {
    sudo sed -i '$ a\4000:raw:0:/dev/ttyUSB0:9600 NONE 1STOPBIT 8DATABITS' /etc/ser2net.conf
    sudo /etc/init.d/ser2net restart
  }

  # rotate rpi 7" screen
  #grep -s lcd_rotate /boot/config.txt || {
  #  sudo sed -i '1s/^/lcd_rotate=2\n/' /boot/config.txt
  #}
  
EOF
}

config_disable_consoleblank() {
  read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
  NEWHOST=${PROMTPHOST:-$DEFAULTHOST}

  ssh -q -i ~/.ssh/id_rsa pi@$NEWHOST.local <<EOF
  set -x

  # disable_consoleblank
  
  sudo grep -s "consoleblank=0" /boot/cmdline.txt && echo "consoleblank is disabled" || {
    sudo sed -i '1 s/$/ consoleblank=0/' /boot/cmdline.txt
  }
  
EOF
}

config_doctor() {
  read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
  NEWHOST=${PROMTPHOST:-$DEFAULTHOST}

  ssh -q -i ~/.ssh/id_rsa pi@$NEWHOST.local <<EOF
  #set -x
  
  hash node 2> /dev/null && echo "node installed" || echo "node is not installed"
  
EOF
}

#ExecStart=-/sbin/agetty --autologin pi --noclear %I $TERM
config_connect() {
  read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
  NEWHOST=${PROMTPHOST:-$DEFAULTHOST}
  until ping -c1 $NEWHOST.local &>/dev/null; do :; done

  ssh -i ~/.ssh/id_rsa pi@$NEWHOST.local
}

run_setup() {
  if [ -z $NEWHOST ]; then
    read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
    NEWHOST=${PROMTPHOST:-$DEFAULTHOST}
  fi
  # echo $1
  cat ./setup/$1.sh  | ssh -q -i ~/.ssh/id_rsa pi@$NEWHOST.local
}

config_all() {
  read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
  export NEWHOST=${PROMTPHOST:-$DEFAULTHOST}
  
  run_setup node
  run_setup vnc
  run_setup enable-spi
  run_setup disable-console-blank
  run_setup miniPiTFT
  run_setup indi
  run_setup indi-manager
  run_setup phd2
  run_setup kstars
  run_setup astrometry
  run_setup gsc
  
}

config_upgrade() {
  read -p "new hostname [$DEFAULTHOST]:" PROMTPHOST
  export NEWHOST=${PROMTPHOST:-$DEFAULTHOST}
  
  run_setup update
  run_setup miniPiTFT
  run_setup indi
  run_setup indi-manager
  run_setup phd2
  run_setup gsc
}

config_help() {
  # COMMANDLIST=$(compgen -A function | sed "s/config_\(.*\)/    \1/")
  cat <<EOF
scopetablet installation and configuration.
USAGE: ./setup [command]

For a new pi image, run 'setup wifi' while the card is connected to the macbook.

After booting the pi, use 'setup ssh' to configure ssh to use a certificate to login
and disable password login via ssh.

  COMMANDS:
    wifi
    ssh
    connect
    disable_consoleblank
    doctor
    help
    
EOF
}

DEFAULTHOST=picam4

[ -z $1 ] && config_help || [ -f ./setup/$1.sh ] && run_setup $* || config_$1 $*